from setuptools import setup

install_requires = ['flask', 'pprint', 'websocket-client','requests']
console_script = ['pser=client:main']

setup(
        name='phidgetrestclient',
        py_modules=['client'],
        version='0.2.0',
        install_requires=install_requires,
        url='https://gitlab.com/elrichindy/phidgetrestclient',
        license='MIT',
        author='Elric Hindy',
        author_email='elrichindy@gmail.com',
        description='client for phidgetrest server',
        entry_points={
            'console_scripts': console_script,
            },
)

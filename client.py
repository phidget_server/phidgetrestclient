#!/usr/bin/env python
import sys
import os

import requests
from requests.auth import HTTPBasicAuth
from websocket import create_connection
import websocket

try:
    import configparser as cp
except ImportError:
    import ConfigParser as cp

import argparse
from flask import json

from pprint import PrettyPrinter

if sys.version_info >= (3, 0):
    raw_input = input


authority = None


def pretty_print(this):
    # get_terminal_size only works on python 3.3+
    try:
        p = PrettyPrinter(width=os.get_terminal_size().columns - 30)
    except AttributeError:
        p = PrettyPrinter(width=80)
    p.pprint(this)


def ws_run(args):
    def on_error(ws, error):
        print(error)

    def on_message(ws, msg):
        pretty_print(msg)

    def on_close(ws):
        print("WebSocket Closed")

    ws_url = 'ws://{}:{}/websocket/'.format(args.wsaddress, args.wsport)
    auth = HTTPBasicAuth(args.token, '')
    headers = {'content-type': 'application/json'}
    # websocket.enableTrace(True)
    print('Starting websocket receive from {}'.format(ws_url))
    ws = websocket.WebSocketApp(ws_url, header=headers, on_error=on_error, on_message=on_message, on_close=on_close)
    ws.run_forever(http_proxy_auth=auth)
    return


def set_config(args=None, config_path=None):
    if config_path is None:
        config_path = args.config_path
    cfg = cp.ConfigParser()
    host_add = raw_input('Enter Host Address(default localhost): ') or 'localhost'
    host_port = int((raw_input('Enter Host Port(default 5100): ')) or 5100)
    ws_host = raw_input('Enter Websocket Host(default {}): '.format(host_add)) or host_add
    ws_port = int(raw_input('Enter Websocket Port(default {}): '.format(host_port + 2)) or host_port + 2)

    section = 'Settings'
    cfg.add_section(section)
    cfg.set(section, 'WS_PORT', str(ws_port))
    cfg.set(section, 'WS_HOST', str(ws_host))
    cfg.set(section, 'HOST_PORT', str(host_port))
    cfg.set(section, 'HOST', str(host_add))

    with open(config_path, 'w') as configfile:
        cfg.write(configfile)

    return 'Config Set'


def parse_opts(argv, config_path):
    config = cp.ConfigParser()
    config.read(config_path)
    section = 'Settings'
    try:
        HOST = config.get(section, 'HOST')
        HOST_PORT = config.get(section, 'HOST_PORT')
        WS_HOST = config.get(section, 'WS_HOST')
        WS_PORT = config.get(section, 'WS_PORT')
    except cp.NoSectionError:
        print("Error with config, please recreate:")
        set_config(config_path)
        HOST = config.get(section, 'HOST')
        HOST_PORT = config.get(section, 'HOST_PORT')
        WS_HOST = config.get(section, 'WS_HOST')
        WS_PORT = config.get(section, 'WS_PORT')

    parser = argparse.ArgumentParser(description='Client to send data to phidget rest server',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-a', '--address', default=HOST, type=str, help='address of phidget rest server')
    parser.add_argument('-WA', '--wsaddress', default=WS_HOST, type=str, help='address of phidget websocket server')
    parser.add_argument('-P', '--port', default=HOST_PORT, type=str, help='port of phidget rest server')
    parser.add_argument('-WP', '--wsport', default=WS_PORT, type=str, help='port of phidget websocket server')
    parser.add_argument('-u', '--user', default='administrator', type=str, help='user name')
    parser.add_argument('-A', '--password', default='admin1234', type=str, help='password')
    parser.add_argument('-t', '--token', default='C005DB5E43FA05E2EABA488F37CAB234E7295F59', type=str, help='token')
    parser.set_defaults(func=get_state, device=None)

    subparsers = parser.add_subparsers(title="Commands", description="Commands to send to the Phidget Server")
    monitor_parser = subparsers.add_parser('monitor', help="monitor the server status")
    monitor_parser.set_defaults(func=ws_run)

    get_state_parser = subparsers.add_parser('get_state', help="Get the current state of a device or all devices")
    get_state_parser.add_argument('device', type=str, default=None, help='device/group or configuration to switch')
    get_state_parser.add_argument('-p', '--ports', default=None, nargs='*', type=int, help='list of ports to switch')
    get_state_parser.set_defaults(func=get_state)

    config_parser = subparsers.add_parser('config', help="Configure server parameters")
    config_parser.set_defaults(func=set_config, config_path=config_path)
    command_parser = subparsers.add_parser('set', help='Sets the output of the device')
    sub_command_parsers = command_parser.add_subparsers(title='control options',
                                                        description='Different control options')
    open_command_parser = sub_command_parsers.add_parser('open', help='Sets the device ports to open')
    open_command_parser.add_argument('device', type=str, default=None, help='device/group or configuration to switch')
    open_command_parser.add_argument('-p', '--ports', default=None, nargs='*', type=int, help='list of ports to switch')
    open_command_parser.set_defaults(func=set_command, command='set_open')

    close_command_parser = sub_command_parsers.add_parser('close', help='Sets the device ports to close')
    close_command_parser.add_argument('device', type=str, default=None, help='device/group or configuration to switch')
    close_command_parser.add_argument('-p', '--ports', default=None, nargs='*', type=int,
                                      help='list of ports to switch')
    close_command_parser.set_defaults(func=set_command, command='set_closed')

    toggle_command_parser = sub_command_parsers.add_parser('toggle', help='Sets the device ports to toggle')
    toggle_command_parser.add_argument('device', type=str, default=None, help='device/group or configuration to switch')
    toggle_command_parser.add_argument('-p', '--ports', default=None, nargs='*', type=int,
                                       help='list of ports to switch')
    toggle_command_parser.set_defaults(func=set_command, command='set_toggle')

    cycle_command_parser = sub_command_parsers.add_parser('cycle', help='Sets the device ports to cycle')
    cycle_command_parser.add_argument('device', type=str, default=None, help='device/group or configuration to switch')
    cycle_command_parser.add_argument('-p', '--ports', default=None, nargs='*', type=int,
                                      help='list of ports to switch')
    cycle_command_parser.add_argument('-c', '--cycles', default=1, type=int, help='number of cycles')
    cycle_command_parser.add_argument('-i', '--input', default=None, type=str, help='file containing timing data')
    cycle_command_parser.add_argument('-tc', '--timeclosed', default=1, type=float, help='time on for cycles')
    cycle_command_parser.add_argument('-to', '--timeopen', default=1, type=float, help='time off for cycles')
    cycle_command_parser.add_argument('-s', '--statestart', default='set_open', type=str,
                                      choices=['set_open', 'set_closed'], help='state cycles begin')
    cycle_command_parser.add_argument('-e', '--stateend', default='set_open', type=str,
                                      choices=['set_open', 'set_closed'], help='state cycles end')
    cycle_command_parser.set_defaults(func=cycle_command)

    add_config_parser = subparsers.add_parser('add', help="Add configurations")
    sub_add_config_parser = add_config_parser.add_subparsers(title='Add device/server configurations')
    device_config_parser = sub_add_config_parser.add_parser('device', help='Add a device configuration')
    device_config_parser.add_argument('-d', '--data', type=str, default=None, help='json formatted string for data')
    device_config_parser.set_defaults(func=add_device)

    task_parser = subparsers.add_parser('task', help='Task commands')
    sub_task_parsers = task_parser.add_subparsers(title='task options',
                                                  description='Different Task Options')
    get_task_parser = sub_task_parsers.add_parser('get', help='Gets the current running tasks uuids')
    get_task_parser.set_defaults(func=get_tasks, command='get_tasks')

    cancel_task_parser = sub_task_parsers.add_parser('cancel', help='Cancels a running task')
    cancel_task_parser.add_argument('-U', '--uuid', type=str, default=None, help='uuid of task')
    cancel_task_parser.set_defaults(func=cancel_tasks, command='cancel_task')

    raw_data_parser = subparsers.add_parser('raw', description="Send raw json data to server", help="Raw data to parse")
    raw_data_parser.add_argument('data', type=str, default=None, help='json formatted string for all other commands')

    args = parser.parse_args(argv)
    global authority
    authority = HTTPBasicAuth(args.token, '')
    return args


def send(url, headers=None, auth=None, data=None, method='POST'):
    if headers is None:
        headers = {'content-type': 'application/json'}
    if auth is None:
        auth = authority

    if method == 'POST':
        res = requests.post(url, json=data, headers=headers, auth=auth)  # type: requests.Response
    else:
        res = requests.get(url, headers=headers, auth=auth)  # type: requests.Response
    result_raw = res.text.encode()
    try:
        result = res.json()
    except ValueError:
        result = result_raw
    return result


def parse_script_file(filename):
    time_open_list = []
    time_closed_list = []
    file_path = os.path.expanduser(filename)
    with open(file_path, 'r') as f:
        for line in f:
            try:
                time_open, time_closed = line.strip().split(',')
            except ValueError:
                continue
            time_open_list.insert(0, int(time_open))
            time_closed_list.insert(0, int(time_closed))
    return time_open_list, time_closed_list


def get_input(message):
    """
    Get input and check for quit or save
    :param message: Message to display
    :type message: str
    :return: response or False if save and True if quit 
    :rtype: str or bool or None)
    """
    res = input(message)
    if 'q!' in res.lower():
        return True
    elif 'w!' in res.lower():
        return False
    else:
        return res


def set_command(args):
    url = 'http://{}:{}/control/'.format(args.address, args.port)

    try:
        device, ports = args.device.split(':', 1)
    except ValueError:
        device = args.device
        ports = args.ports

    data = {
        'device': device,
        'action': args.command,
    }

    if ports is not None:
        data.update({'ports': ports})

    return send(url, data=data)


def cycle_command(args):
    url = 'http://{}:{}/control/'.format(args.address, args.port)
    try:
        device, ports = args.device.split(':', 1)
    except ValueError:
        device = args.device
        ports = args.ports

    if args.input is not None:
        try:
            time_open, time_closed = parse_script_file(args.input)
        except IOError:
            return "Error parsing file {0}".format(args.input)
        if not time_open or not time_closed:
            return "No data in script file {0}".format(args.input)
    else:
        time_open = args.timeopen
        time_closed = args.timeclosed

    data = {
        'device': device,
        'cycles': args.cycles,
        'action': args.command,
        'time_open': time_open,
        'time_closed': time_closed,
        'state_start': args.statestart,
        'state_end': args.stateend
    }

    if ports is not None:
        data.update({'ports': ports})

    return send(url, data=data)


def add_device(args):
    url = 'http://{}:{}/deviceconfigurations/'.format(args.address, args.port)

    if args.data is None:
        args.data = {}
        device_name = raw_input("Enter a name for this device: ")
        args.data.update({"name": device_name})
        args.data.update({"configuration": {}})
        dev_config = args.data.get("configuration")
        while True:
            serial = raw_input("Enter Serial of Phidgets Device: ")
            dev_config.update({serial: {}})
            serial_config = dev_config.get(serial)
            while True:
                port_name = raw_input("Enter name for port: ")
                ports = []
                while True:
                    port_number = raw_input("Enter the port number: ")
                    ports.append(int(port_number))
                    if 'n' in raw_input("Add another port number y/n: ").lower():
                        break
                serial_config.update({port_name: ports})
                if 'n' in raw_input("Add another port y/n: ").lower():
                    break
            print("Review Data to Send: ")
            pretty_print(args.data)
            while True:
                res = raw_input("Q/q to quit, Y/y to send to server: ")
                if 'q' in res.lower():
                    print("Quitting add device configuration")
                    return
                elif 'y' in res.lower():
                    print("Sending configuration to server")
                    return send(url, data=args.data, method='POST')
                else:
                    print("{} is not a valid response".format(res))
    else:
        return send(url, data=json.loads(args.data), method='POST')


def get_state(args):
    url = 'http://{}:{}/control/'.format(args.address, args.port)
    if args.device is not None:
        url = '{}{}/'.format(url, args.device)

    return send(url, method='GET')


def get_tasks(args):
    data = {
        'action': args.command,
    }
    url = 'http://{}:{}/tasks/'.format(args.address, args.port)
    return send(url, data=data, method='GET')


def cancel_tasks(args):
    data = {
        'action': args.command,
        'uuid': args.uuid
    }
    url = 'http://{}:{}/tasks/'.format(args.address, args.port)
    return send(url, data=data)


def add_server(args):
    if args.data is None:
        server_address = input("Enter Server Address: ")
        server_port = input("Enter Server Port: ")
        server_name = input("Enter Server Name: ")
        name = input("Enter Unique Name: ")
        data = {
            'address_port': {
                'address': server_address,
                'port': int(server_port)
            },
            'server_name': server_name,
            'name': name,
            'enabled': 'yes'
        }
    else:
        data = json.loads(args.data)

    url = 'http://{}:{}/servers/'.format(args.address, args.port)

    return send(url, data=data, method='POST')


def run(argv):
    config_path = os.path.join(os.path.expanduser('~'), '.pserc')
    if not os.path.exists(config_path):
        print("No Configuration File, please create defaults")
        print(set_config(config_path=config_path))

    args = parse_opts(argv, config_path)
    return args.func(args)


def main():
    argv = sys.argv[1:]
    try:
        result = run(argv)
        if result is not None:
            pretty_print(result)
    except KeyboardInterrupt:
        print('Exiting Client...')


if __name__ == '__main__':
    main()
